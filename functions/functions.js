const fs = require('fs');
const axios = require('axios');

/* Converts degrees fahenheit to degrees celcius */
var toCelcius = (degrees) => {
    return (degrees - 32) * (5 / 9);
};

/* Add a default address */
var addDefaultAddress = (address) => {
    var file = [];
    file.push(address);
    saveFile(file);

    return;
};

/* Save the file */
var saveFile = (file) => {
    fs.writeFileSync('default.json', JSON.stringify(file));

    return;
};

/* Fetch default address if it exists */
var getDefaultAddress = () => {
    return new Promise((resolve, reject) => {
        var address = fetchAddress();

        if (address[0] == null) {
            reject('There is no default address!  Please add one with -d when entering a location with -a.');
        } else {
            resolve(address[0]);
        }
    });
};

/* Fetch coordinates from file */
var fetchAddress = () => {
    try {
        return JSON.parse(fs.readFileSync('default.json'));

    } catch (e) { 
        return [];
    }
};

/* Process data and return results */
var processData = (geocodeURL, encodedAddress) => {
    const weatherKey = '71b7dcbad85b2e1c1a5836b01f45d026';
    var displayCelcius = false;
    var latitude, longitude;

    axios.get(geocodeURL).then((response) => {
        if (response.data.resourceSets[0].estimatedTotal < 1) {
            throw new Error('Unable to find that address.');
        }
    
        latitude = response.data.resourceSets[0].resources[0].point.coordinates[0];
        longitude = response.data.resourceSets[0].resources[0].point.coordinates[1];
        
        var weatherURL = `https://api.darksky.net/forecast/${weatherKey}/${latitude},${longitude}`;
    
        console.log(response.data.resourceSets[0].resources[0].name);
    
        return axios.get(weatherURL);
    }).then((response) => {
        var temperatureType = 'Fahrenheit';
    
        var temperature = response.data.currently.temperature;
        var apparentTemperature = response.data.currently.apparentTemperature;
        var summary = response.data.hourly.summary;
        summary = summary.toLowerCase();
    
        /* Check for presence of non-value flags */
        process.argv.forEach((val) => {
            if (val === '-c' || val === '--celcius') {
                displayCelcius = true;
            }
    
            if (val === '-d' || val === '--default') {
                // Add encodedAddress to default.txt
                addDefaultAddress(encodedAddress);
            }
        });
    
        if (displayCelcius) {
            
            /* Convert to celcius */
            temperature = toCelcius(temperature);
            apparentTemperature = toCelcius(apparentTemperature);
            temperatureType = 'Celcius';
        }
    
        /* Limit to 2 decimal places */
        temperature = temperature.toFixed(2);
        apparentTemperature = apparentTemperature.toFixed(2);
    
        console.log(`It's currently ${temperature}\u00B0 ${temperatureType}, but it feels like ${apparentTemperature}\u00B0 ${temperatureType}.`); // \u00B0 add the degree symbol
        console.log(`You can expect it to be ${summary}`);
    }).catch((e) => {
        if (e.code === 'ENOTFOUND') {
            console.log('Unable to connect to API servers.');
        } else {
            console.log(e.message);
        }
    });
}


module.exports = {
    getDefaultAddress,
    processData
}